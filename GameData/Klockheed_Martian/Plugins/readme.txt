Klockheed Martian General Plugin Functions
--------------------------------------------------------------
Source: https://github.com/dtobi

This collection of plugins is required to run Klockheed_Martian Plugins. It provides service functions for pligins like:

Asteroid Cities
Space Shuttle Engines
Klockheed Martian Smart Parts
Klockheed Martian Special Parts

Please update this file every time you update another Klockheed Martian Mod.
The files in this mod are compatible with Klockheed Martian mods above version 2.0. Please remove all Klockheed Martian mods with version numbers smaller than 2.0. 

------------------------------
This work is shared under Creative Commons CC BY-NC-ND 3.0 license.
You may non-commercially share it as-is with credit to the author in the download and on the download page.
Please contact me if you want to modify or extend the code.
Author: dtobi
------------------------------
